#include <iostream>

void printNumbers(int maxNumber, bool even = true)
{
    for (int i = 0; i < maxNumber; i++)
    {
        if (i % 2 == !even) {
            std::cout << i << std::endl;
        }
    }
}

int main() {
    const int maxNumber = 50;
    printNumbers(maxNumber, true);
}